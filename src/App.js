/* 
** INCMusic Player
** Built on React, Redux, Material-UI, and Howler.js
*/

import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


import MusicAppBar from './comp/MusicAppBar';
import MusicDrawer from './comp/MusicDrawer';
import MusicBrowser from './comp/MusicBrowser';
import MusicPlayer from './comp/MusicPlayer';
import LyricsModal from './comp/subcomp/LyricsModal';

const theme = createMuiTheme({
	palette: {
		primary: {
			light: "#e06363",
			main : "#c62828",
			dark : "#7f1a1a",
			contrastText: '#fff',
		},
		secondary: {
			light: "#718792",
			main : "#455a64",
			dark: "1c313a",
			contrastText: '#fff'
		},
		values: {
			md:768,
		},
		text: {
			primary: "#444444",
		}
	}
});


class App extends Component {
	constructor(props){
		super(props);

		this.state = {
			drawerOpen: false
		}

		this.handleDrawer = this.handleDrawer.bind(this);
	}
	
	handleDrawer = () =>{
		var tState = !this.state.drawerOpen;
		this.setState({
			drawerOpen : tState
		})
	}

	render() {
		return (
			<MuiThemeProvider theme={theme}>
				<div className="app">
					<CssBaseline />
					
					<MusicAppBar history={this.props.history} handleDrawer={this.handleDrawer} />
					<MusicDrawer handleDrawer={this.handleDrawer} drawerOpen={this.state.drawerOpen} />
					<LyricsModal />
					<MusicBrowser drawerOpen={this.state.drawerOpen} />
					
					<MusicPlayer />
				</div>
			</MuiThemeProvider>
		);
	}
}

export default App;
