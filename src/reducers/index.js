import update from 'immutability-helper';
import * as Fuse from 'fuse.js';

const emptySong = {
    idSongs : 0,
    idGenre : 0,
    idCategory : 0,
    idLyrics : null,
    idAlbum : 0,
    idAlbumContent : 0,
    songAuthor : "",
    songTitle : "",
    albumTitle : "",
    artUrl : "",
    songUrl : "",
    videoUrl : "",
    bpm : 0,
    dateRelease : "",
    len : 0
}


var options = {
    shouldSort: true,
    threshold: 0.2,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 4,
    keys: ["songTitle"]
}


const initialState = {
    isPlaying: true,
    currentSong: emptySong,
    playlist: [],
    currentIndex: 0,
    repeat: false,
    allSongs:[],
    allAlbums:[],
    searchQuery: "",
    searchResults: [],
    idLyrics:0,
    lyricsOpen: false,
    lyricsHTML:""
};




const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PLAY_SONG':
            window.wavesurfer.load(action.payload.songUrl)
            return update(state, {
                currentSong: {$set: action.payload},
                isPlaying : {$set: true},
            })
        case 'ADD_PLAYLIST':
            return update(state, {
                playlist: {$push: action.payload}
            })
        case 'ORDER_PLAYLIST':
            var newIndex = action.payload.findIndex(o =>{ return o.idSongs === state.currentSong.idSongs})
            return {...state, playlist: action.payload, currentIndex: newIndex}
        case 'CLEAR_PLAYLIST':
            window.wavesurfer.empty()
            return update(state, {
                currentSong: {$set: emptySong},
                playlist: {$set: []},
                currentIndex: {$set: 0},
                isPlaying: {$set: false}
            })
        case 'REMOVE_SONG':
            return update(state,
                    
            )
        case 'CHANGE_INDEX':
            return update(state, {
                currentIndex : {$set: action.payload}
            })
        case 'TOGGLE_PLAY':
            window.wavesurfer.play()
            return update(state, {
                isPlaying: {$set: true}
            })
        case 'TOGGLE_PAUSE':
            window.wavesurfer.pause()
            return update(state, {
                isPlaying: {$set: false}
            })
        case 'TOGGLE_REPEAT':
            return update(state, {
                repeat: {$set: !state.repeat}
            })
        case 'LOAD_SONGS':
            window.fuse = new Fuse(action.payload, options)
            return {...state,allSongs : action.payload}
        case 'LOAD_ALBUMS':
            return {...state,allAlbums : action.payload}
        case 'SEARCH':
            if(action.payload.length ===0){
                return update(state, {
                    searchQuery:{$set: action.payload},
                    searchResults: {$set: []}
                })
            }
            if(action.payload.length <3){
                return update(state, {
                    searchQuery:{$set: action.payload},
                })
            }
            
            var searchResults = window.fuse.search(action.payload);
            return update(state, {
                searchQuery:{$set: action.payload},
                searchResults: {$set: searchResults}
            })
        case 'SHOW_LYRICS':
            return update(state, {
                idLyrics: {$set: action.payload},
                lyricsOpen: {$set: true}
            })
        case 'HIDE_LYRICS':
            return update(state, {
                idLyrics: {$set: 0},
                lyricsOpen: {$set: false}
            })
        default:
            return state;
    }
};

export default rootReducer;