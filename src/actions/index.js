export const playSong = song => {
    return { type: 'PLAY_SONG', payload: song};
};

export const addPlaylist = song => {
    return { type: 'ADD_PLAYLIST', payload: song};
}

export const orderPlaylist = playlist => {
    return { type: 'ORDER_PLAYLIST', payload: playlist};
}

export const removeSong = playlist =>{
    return { type: 'REMOVE_SONG', payload:playlist}
}

export const clearPlaylist = playlist => {
    return { type: 'CLEAR_PLAYLIST', payload: playlist};
}

export const changeIndex = index => {
    return { type: 'CHANGE_INDEX', payload: index};
}

export const togglePlay = load => {
    return { type: 'TOGGLE_PLAY', payload: load};
};

export const togglePause = load => {
    return { type: 'TOGGLE_PAUSE', payload:load};
};

export const toggleRepeat = load => {
    return { type: 'TOGGLE_REPEAT', payload:load};
};

export const loadSongs = load => {
    return { type: 'LOAD_SONGS', payload:load};
}

export const loadAlbums = load => {
    return { type: 'LOAD_ALBUMS', payload:load};
}

export const search = searchQuery => {
    return { type: 'SEARCH', payload:searchQuery };
}

export const showLyrics = idLyrics => {
    return { type: 'SHOW_LYRICS', payload:idLyrics };
}

export const hideLyrics = idLyrics => {
    return { type: 'HIDE_LYRICS', payload:idLyrics };
}