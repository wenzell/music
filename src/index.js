import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import $ from 'jquery';
import { Provider } from 'react-redux';
import store from './store/index';

window.rootUrl = "http://104.130.135.129/api/";
window.noArtUrl = "https://d607d9adb89cabd66253-1422a07285cc35aa1c8781082aaa4295.ssl.cf1.rackcdn.com/music/incmusic-default-album250px.png"


const browserHistory = createHistory();

browserHistory.listen((location,action)=>{
    $('html,body').animate({ "scrollTop": 0 }, 400);
})

ReactDOM.render(<Provider store={store}><Router history={browserHistory} ><App history={browserHistory} /></Router></Provider>, document.getElementById('root'));
registerServiceWorker();
