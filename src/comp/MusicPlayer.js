import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Slider from '@material-ui/lab/Slider';

import RepeatIcon from '@material-ui/icons/Repeat';
import RepeatOneIcon from '@material-ui/icons/RepeatOne';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import ShuffleIcon from '@material-ui/icons/Shuffle';
import QueueMusicIcon from '@material-ui/icons/QueueMusic';
import ArrowDropIcon from '@material-ui/icons/ArrowDropUp';

import MusicMenu from './subcomp/MusicMenu';
import MusicSongGroup from './subcomp/MusicSongGroup';
import MusicWave from './subcomp/MusicWave';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions/index.js';







const styles= {
    root:{
        width:'320px',
        top:'64px',
        height:'calc(100vh - 64px)',
        userSelect:'none',
        '@media screen and (max-width:768px)':{
            top:'calc(100vh - 179px)',
            left:'0',
            right:'0',
            width:'initial',
            borderTop: 'solid 1px rgba(0,0,0,0.1)',
            overflowY:'initial',
            transition:'0.3s',
        },
    },
    up:{
        top:'64px'
    },
    albumContainer:{
        backgroundSize:'cover',
        height:'160px',
        width:'320px',
        background:'center',
        transition:'0.5s',
        '@media screen and (min-height:850px)':{
            height: '320px'
        },
        '@media screen and (max-width:768px)':{
            height: '0'
        },
        
    },
    info:{
        display:'flex',
        width:'100%',
        height:'70px',
        padding:'5px 10px 5px 20px',
        alignItems:'center'
    },
    songInfo:{
        display:'flex',
        flexFlow:'column',
        width:'80%'
    },
    moreIcon:{
        marginLeft:'auto'
    },
    player:{
        width:'100%',
    },
    controls:{
        width:'100%',
        display:'flex',
        alignItems:'center',
        justifyContent:'space-between',
        padding:'0px 20px'
    },
    playButton:{
        fontSize:'2em',
        transition:'0.2s',
        cursor:'pointer',
        '&:hover':{
            filter: "brightness(1.2) saturate(0.8)"
        }
    },
    volume:{
        padding:'0px 50px 10px 50px'
    },
    playlistheader:{
        display:'flex',
        width:'100%',
        padding:'10px 10px 10px 20px',
        alignItems:'center'
    },
    playlistInfo:{
        display:'flex',
    },
    playlisttitle:{
        fontSize:'1em',
        lineHeight:'1em'
    },
    playlist:{
        padding:'10px 5px 10px 10px',
        overflow:'auto',
        flex:1,
    },
    popbutton:{
        width:'100%',
        display:'flex',
        height:'16px',
        position:'absolute',
        justifyContent:'center',
        '@media screen and (min-width:768px)':{
            display: 'none'
        }
    },
    flip:{
        transform:'rotate(180deg)'
    }


    
};

class MusicPlayer extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            
            sliderValue: 10,
            isPlaying: this.props.isPlaying,
            drawerUp:false,
        }
    }



    //Volume Slider Change
    handleSliderChange = (event, value) =>{
        this.setState({sliderValue : value})
        window.wavesurfer.setVolume(value/10)
    }

   
    handleDrawer = () =>{
        this.setState({drawerUp:!this.state.drawerUp})
    }



    //Music Controls
    handlePlayClick = () =>{
            if(this.props.isPlaying){
                this.props.togglePause(0)
            }
            else{
                this.props.togglePlay(0)
            }
        

        
    }

    handleNext = () =>{
        if(this.props.playlist.length !== 0){
            this.props.playSong(this.props.playlist[ this.props.currentIndex === this.props.playlist.length-1 ? 0 : this.props.currentIndex + 1  ])
        }
    }

    handlePrevious = () =>{
        if(window.wavesurfer.getCurrentTime() < 3){
            if(this.props.playlist.length !== 0){
                if(!this.props.playlist.some(o=>{return o.idSongs === this.props.currentSong.idSongs})){
                    this.props.playSong(this.props.playlist[0])
                }
                else{
                    this.props.playSong(this.props.playlist[ this.props.currentIndex === 0 ? this.props.playlist.length - 1 : this.props.currentIndex - 1  ])
                }
            }else{
                window.wavesurfer.seekTo(0)
            }
        }
        else{
            window.wavesurfer.seekTo(0)
        }
    }

    handleRepeatClick = () =>{
        this.props.toggleRepeat(0)
    }

    handleShuffleClick= () =>{
        if(this.props.playlist.length !== 0){
            var shuffle  = (array) =>{
                var newArray = array;
                var i = 0
                , j = 0
                , temp = null
    
                for (i = newArray.length - 1; i > 0; i -= 1) {
                    j = Math.floor(Math.random() * (i + 1))
                    temp = newArray[i]
                    newArray[i] = newArray[j]
                    newArray[j] = temp
                }
                return newArray;
            }
    
            var shuffledPlaylist = shuffle(this.props.playlist);
            this.props.orderPlaylist(shuffledPlaylist)
            this.props.playSong(this.props.playlist[0])
    
        }
        

    }

    render(){
        var classes = this.props.classes;
        return(
            <Drawer classes={{paper:classnames(classes.root,this.state.drawerUp ? classes.up : '')}} anchor="right" open variant="persistent">
                <div className={classnames(classes.popbutton, this.state.drawerUp ? classes.flip : '')} onClick={this.handleDrawer}>
                    <ArrowDropIcon style={{color:'#aaa'}}/>
                </div>

                <div style={{backgroundImage:"url(" +  (this.props.currentSong.artUrl !== "" ? this.props.currentSong.artUrl : '')  +  ")"}} className={classes.albumContainer}></div>
                <div className={classes.info}>
                    <div className={classes.songInfo}>
                        <Typography variant="title" color="primary" style={{fontSize:this.props.currentSong.songTitle.length > 19 ? '1em': '1.3125em'}}>{this.props.currentSong.songTitle}</Typography>
                        <Typography variant="caption" color="default" noWrap>{this.props.currentSong.albumTitle}</Typography>
                    </div>
                    {this.props.currentSong.songTitle.length !== 0 ?
                    <MusicMenu playerSong className={classes.moreIcon} song={this.props.currentSong} /> : null
                }
                    
                </div>
                <div className={classes.player}>
                    <MusicWave />
                </div>
                <div className={classes.controls}>
                    <IconButton color="primary" onClick={this.handleRepeatClick}>
                        {this.props.repeat? <RepeatOneIcon /> : <RepeatIcon />}
                    </IconButton>
                    <IconButton color="primary" onClick={this.handlePrevious}>
                        <SkipPreviousIcon />
                    </IconButton>
                    <IconButton color="primary" onClick={this.handlePlayClick}>
                    {!this.props.isPlaying ? <PlayCircleFilledIcon className={classes.playButton} color="primary" /> : <PauseCircleFilledIcon className={classes.playButton} color="primary" />}
                    </IconButton>

                    <IconButton color="primary" onClick={this.handleNext}>
                        <SkipNextIcon />
                    </IconButton>
                    <IconButton color="primary" onClick={this.handleShuffleClick}>
                        <ShuffleIcon />
                    </IconButton>
                </div>
                <div className={classes.volume}>
                    <Slider value={this.state.sliderValue} min={0} max={10} step={1} onChange={this.handleSliderChange} />
                </div>
                <Divider />
                <div className={classes.playlistheader}>
                    <QueueMusicIcon color="primary" style={{fontSize:36}}/>
                    <div className={classes.playlistinfo}>
                        <Typography className={classes.playlisttitle} color="primary" variant="body2">Current Playlist</Typography>
                        <Typography variant="caption">{this.props.playlist.length} song{this.props.playlist.length > 1 || this.props.playlist.length === 0 ? 's' : '' }</Typography>
                    </div>
                    <MusicMenu className={classes.moreIcon} playlistControl color="primary" />
                </div>
                <div className={classes.playlist}>
                    <MusicSongGroup isPlaylist album={this.props.playlist} small />
                </div>
            </Drawer>
            
        )
    }
}

function mapStateToProps(state){
	return {
        isPlaying: state.isPlaying,
        currentSong: state.currentSong,
        repeat: state.repeat,
        playlist: state.playlist,
        currentIndex: state.currentIndex
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            playSong: Actions.playSong,
            togglePlay: Actions.togglePlay,
            togglePause: Actions.togglePause,
            toggleRepeat: Actions.toggleRepeat,
            orderPlaylist: Actions.orderPlaylist
		}, dispatch)
}



export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(MusicPlayer)));