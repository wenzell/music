import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Fade';
import Helmet from 'react-helmet';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';

import PlayIcon from '@material-ui/icons/PlayCircleOutline';

import MusicSongGroup from '../subcomp/MusicSongGroup';

const styles= {
    root:{
        display:'flex',
        flexFlow:'column',
        position:'relative',
        alignItems:'center',
    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px'
    },
    header:{
        display: 'block',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '-20px',
        left:'-20px',
        right:'-20px',
        height: '350px',
        overflow:'hidden',
        zIndex: '-1',
        filter:'blur(10px)',
        '@media screen and (max-width: 768px)':{
            height:'211px',
            right:'-20px'
        }
    },
    rootPaper:{
        margin:'250px 5%',
        width:'98%',
        maxWidth:'850px',
        padding:'20px',
        '@media screen and (max-width: 768px)':{
            margin:'180px 1% 100px 1%',
        }
    },
    albumhead:{
        display:'flex',
        width:'100%',
        alignItems:'flex-end',
        '@media screen and (max-width:768px)':{
            alignItems: 'center'
        }
    },
    albumimage:{
        width:'120px',
        height:'120px',
        marginRight: '20px',
        '@media screen and (max-width:768px)':{
            width: '100px',
            height: '100px',
            marginRight:'10px'
        }
    },
    albuminfo:{
        display:'flex',
        flexFlow:'column',
        '& > h1':{
            fontWeight:300,
            '@media screen and (max-width:768px)':{
                fontSize: '1.5em'
            }
        },
        '& > h3':{
            '@media screen and (max-width:768px)':{
                fontSize: '0.75em'
            }
        }
    },
    divider:{
        margin: '40px -20px',
    },
    playbutton:{
        marginLeft:'auto',
        alignSelf:'flex-start',
        '@media screen and (max-width:768px)':{
            marginRight: '10px'
        }
    }
};

class AlbumPage extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            albumList:[],
            artUrl: '',
            albumTitle:'',
            noArtUrl: 'https://d607d9adb89cabd66253-1422a07285cc35aa1c8781082aaa4295.ssl.cf1.rackcdn.com/music/incmusic-default-album250px.png',
        }
    }

    componentDidMount = () =>{
        axios.get(window.rootUrl+'songs?albumId=' + this.props.match.params.albumid).then(res=>{
            const albumList = res.data;
            const artUrl = albumList[0].artUrl;
            const albumTitle = albumList[0].albumTitle;
            this.setState({ albumList, artUrl, albumTitle })
        })
    }


    playAlbum = () =>{
        this.props.addPlaylist(this.state.albumList)
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.root}>
                <Helmet>
                    <title>{this.state.albumTitle} | INCMusic</title>
                </Helmet>
                <div className={classes.header} style={{backgroundImage: 'url('+(this.state.artUrl !== "" ? this.state.artUrl : this.state.noArtUrl)+'), url('+this.state.noArtUrl +')'}}></div>
                <Slide direction="up" in={true} style={{ transitionDelay: 100 }} mountOnEnter unmountOnExit>
                <Paper className={classes.rootPaper}>
                    <div className={classes.albumhead}>
                        <img alt={this.state.albumTitle} className={classes.albumimage} src={(this.state.artUrl !== "" ? this.state.artUrl : this.state.noArtUrl)} onError={(e)=>{e.target.src=window.noArtUrl}} />
                        <div className={classes.albuminfo}>
                            <Typography variant="display1" color="inherit">{this.state.albumTitle}</Typography> 
                            <Typography variant="subheading">2014 - 2018</Typography>
                        </div>
                        <IconButton className={classes.playbutton} onClick={this.playAlbum} color="primary">
                            <PlayIcon />
                        </IconButton>
                    </div>
                    <Divider className={classes.divider} />
                    <MusicSongGroup type="album" album={this.state.albumList} />
                </Paper>
                </Slide>
            </div>
            
        )
    }
}


function mapStateToProps(state){
	return {
        playlist:state.playlist
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            addPlaylist: Actions.addPlaylist
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(AlbumPage)));