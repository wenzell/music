import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';

import MusicSongGroup from '../subcomp/MusicSongGroup';

const styles= {
    search:{

    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px',
        '@media screen and (max-width:768px)':{
            margin: '0 auto 20px auto'
        }
    },
    paper:{
        width:'80%',
        padding:'10px',
        margin:'auto',
        '@media screen and (max-width:768px)':{
            width: '100%'
        }
    }
};

class name extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            searchResults: []
        }
    }

    componentDidMount = () =>{
        if(this.props.allSongs.length === 0){
            axios.get(window.rootUrl + '/songs/all').then(res =>{
                const allSongs = res.data;
                this.props.loadSongs(allSongs)
            })
        }
    }


    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.search}>
                <Typography className={classes.fontthree} variant="display2">{this.props.searchQuery.length !== 0? "Searching ":"Search"}{this.props.searchQuery}{this.props.searchQuery.length !== 0 ?"...":""}</Typography>
                {this.props.searchResults.length !== 0? 
                <Paper className={classes.paper} elevation={2}>
                    <MusicSongGroup album={this.props.searchResults} />
                </Paper> : null}
            </div>
        )
    }
}

function mapStateToProps(state){
	return {
        searchResults: state.searchResults,
        searchQuery: state.searchQuery,
        allSongs: state.allSongs
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            search: Actions.search,
            loadSongs: Actions.loadSongs
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(name)));