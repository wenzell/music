import React from 'react';
import classnames from 'classnames';
import Grow from '@material-ui/core/Grow';
import Collapse from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withTheme, withStyles } from '@material-ui/core/styles';
import axios from 'axios';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';

import MusicAlbumCard from '../subcomp/MusicAlbumCard';
import MusicSongGroup from '../subcomp/MusicSongGroup';


const styles= {
    root:{
        
    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px',
        '@media screen and (max-width:768px)':{
            margin: '0 auto 20px auto'
        }
    },
    griditem:{
    },
    grid:{
        alignSelf:'center',
        justifyContent:'center',
        width:'100%',
        margin:0,
        marginBottom:'50px',
        transition:'0.5s',
        height:'initial',
        '&.row':{
            justifyContent:'initial',
            flexFlow:'row',
            overflow:'auto',
        },
        '@media screen and (min-width: 1170px)':{
            height:'350px'
        },
        '@media screen and (max-width: 768px)':{
            justifyContent:'space-between',
            height:'initial',
            '&.row':{
                justifyContent:'initial',
                paddingRight:0
            },
        }
    },
    materialgriditem:{
        '@media screen and (max-width:768px)':{
            width:'50%',
        },
        '& > $griditem':{
            '@media screen and (max-width:768px)':{
                width:'100%',
            }
           
        },
    },
    head :{
        display: 'flex',
        justifyContent: 'space-between',
    },
    paper:{
        width:'80%',
        padding:'10px',
        margin:'auto',
        '@media screen and (max-width:768px)':{
            width: '100%'
        }
    }
    
};

class EventsHome extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            isModules: true,
            growEnable:true,
            featuredAlbums:[],
            featuredSongs:[],
        }

    }

    componentDidMount = () =>{
        if(this.props.allSongs.length === 0){
            axios.get(window.rootUrl + 'songs/all').then(res =>{
                const allSongs = res.data;
                this.props.loadSongs(allSongs)
                this.setState({ featuredSongs: allSongs.filter(o=>{return [237,239,235,244,22,15,240,262,254,224].indexOf(o.idSongs) !== -1})} )
            })
        }
        else{
            this.setState({ featuredSongs : this.props.allSongs.filter(o=>{return [237,239,235,244,22,15,240,262,254,224].indexOf(o.idSongs) !== -1}) })
        }
        if(this.props.allAlbums.length === 0){
            axios.get(window.rootUrl + 'albums/').then(res=>{
                const allAlbums = res.data;
                this.props.loadAlbums(allAlbums)
                this.setState({ featuredAlbums: allAlbums.filter(o=>{return [8,9,13,1].indexOf(o.id) !== -1})})
            })
        }
        else{
            this.setState({ featuredAlbums: this.props.allAlbums.filter(o=>{return [8,9,13,1].indexOf(o.id) !== -1})})
        }
        
    }
    
    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.root}>
                <div className={classes.head}>
                    <Typography className={classes.fontthree} variant="display2">Featured Albums</Typography>
                </div>
                <Grid style={{display:this.state.growEnable? 'flex': 'none'}} className={classnames(classes.grid,this.state.isModules ? null : 'row' )} container spacing={24}>
                    
                {this.state.featuredAlbums.map((o,i)=>{
                        return (
                            <Grow key={i} in={this.state.growEnable} style={{transitionDelay: 300 + i*50  }}>
                                <Grid className={classes.materialgriditem} item>
                                    <MusicAlbumCard album={o} classz={classes.griditem} />
                                </Grid>
                            </Grow>
                        )
                    })}
                </Grid>
                <div className={classes.head}>
                    <Typography className={classes.fontthree} variant="display2">Featured Songs</Typography>
                </div>
                <Collapse direction="up" in={this.state.featuredSongs.length !== 0} style={{transitionDelay:400}}>
                <Paper className={classes.paper} elevation={2}>
                <MusicSongGroup album={this.state.featuredSongs} />
                </Paper>
                </Collapse>
                
            </div>
        )
    }
}

function mapStateToProps(state){
	return {
        allSongs: state.allSongs,
        allAlbums: state.allAlbums
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            loadSongs: Actions.loadSongs,
            loadAlbums: Actions.loadAlbums
		}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(EventsHome)));