import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import { withTheme, withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';


import MusicSongGroup from '../subcomp/MusicSongGroup';


const styles= {
    root:{
        display:'flex',
        flexFlow:'column',
        position:'relative',
        alignItems:'center',
    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px',
        '@media screen and (max-width:768px)':{
            margin: '0 auto 20px auto'
        }
    },
    head :{
        display: 'flex',
        justifyContent: 'space-between',
        alignSelf:'flex-start',
        '@media screen and (max-width:768px)':{
            alignSelf: 'initial'
        }
    },

    rootPaper:{
        margin:'0 5%',
        width:'98%',
        maxWidth:'850px',
        padding:'20px',
        '@media screen and (max-width: 768px)':{
            margin:'0 1%',
        }
    },
    albumhead:{
        display:'flex',
        width:'100%',
        alignItems:'flex-end',
        '@media screen and (max-width:768px)':{
            alignItems: 'center'
        }
    },
    albumimage:{
        width:'120px',
        height:'120px',
        marginRight: '20px',
        '@media screen and (max-width:768px)':{
            width: '100px',
            height: '100px',
            marginRight:'10px'
        }
    },
    albuminfo:{
        display:'flex',
        flexFlow:'column',
        '& > h1':{
            fontWeight:300,
            '@media screen and (max-width:768px)':{
                fontSize: '1.5em'
            }
        },
        '& > h3':{
            '@media screen and (max-width:768px)':{
                fontSize: '0.75em'
            }
        }
    },
    divider:{
        margin: '40px -20px',
    },
    playbutton:{
        marginLeft:'auto',
        alignSelf:'flex-start',
        '@media screen and (max-width:768px)':{
            marginRight: '10px'
        }
    }
};

class SongsList extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            isModules: true,
            growEnable:true,
            allSongs:[]
        }
    }

    componentDidMount = () =>{
        if(this.props.allSongs.length === 0){
            axios.get(window.rootUrl + '/songs/all').then(res =>{
                const allSongs = res.data;
                this.props.loadSongs(allSongs)
                this.setState({ allSongs })
            })
        }
        else{
            this.setState({ allSongs : this.props.allSongs})
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.root}>
                <div className={classes.head}>
                    <Typography className={classes.fontthree} variant="display2">Songs</Typography>
                </div>
                <Slide in={this.state.allSongs.length !== 0} direction="up" style={{transitionDelay:100}}>
                <Paper className={classes.rootPaper}>
                    
                    <MusicSongGroup album={this.state.allSongs} />
                </Paper>
                </Slide>
            </div>
        )
    }
}

function mapStateToProps(state){
	return {
		allSongs: state.allSongs
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            loadSongs: Actions.loadSongs
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(SongsList)));