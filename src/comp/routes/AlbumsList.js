import React from 'react';
import classnames from 'classnames';
import Grow from '@material-ui/core/Grow';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';

import MusicAlbumCard from '../subcomp/MusicAlbumCard';


const styles= {
    root:{
        
    },
    fontthree:{
        fontWeight: '300',
        marginBottom:'20px',
        '@media screen and (max-width:768px)':{
            margin: '0 auto 20px auto'
        }
    },
    griditem:{
    },
    grid:{
        alignSelf:'center',
        justifyContent:'center',
        width:'100%',
        margin:0,
        marginBottom:'50px',
        '&.row':{
            justifyContent:'initial',
            flexFlow:'row',
            overflow:'auto',
        },
        
        '@media screen and (max-width: 768px)':{
            justifyContent:'space-between',
            '&.row':{
                justifyContent:'initial',
                paddingRight:0
            },
        }
    },
    materialgriditem:{
        '@media screen and (max-width:768px)':{
            width:'50%',
        },
        '& > $griditem':{
            '@media screen and (max-width:768px)':{
                width:'100%',
            }
           
        },
    },
    head :{
        display: 'flex',
        justifyContent: 'space-between',
    },
};

class AlbumsList extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            isModules: true,
            growEnable:true,
            albums:[]
        }
    }

    componentDidMount = () =>{
        if(this.props.allAlbums.length === 0){
            axios.get(window.rootUrl + '/albums').then(res =>{
                const allAlbums = res.data;
                this.props.loadAlbums(allAlbums)
                this.setState({ albums: allAlbums })
            })
        }
        else{
            this.setState({ albums : this.props.allAlbums})
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.root}>
                <div className={classes.head}>
                    <Typography className={classes.fontthree} variant="display2">Albums</Typography>
                </div>
                <Grid style={{display:this.state.growEnable? 'flex': 'none'}} className={classnames(classes.grid,this.state.isModules ? null : 'row' )} container spacing={24}>
                    
                {this.state.albums.map((o,i)=>{
                        return (
                            <Grow key={i} in={this.state.growEnable} style={{transitionDelay: this.state.growEnable ? 100 + i*50 : 0 }}>
                                <Grid className={classes.materialgriditem} item>
                                    <MusicAlbumCard album={o} classz={classes.griditem} />
                                </Grid>
                            </Grow>
                        )
                    })}
                </Grid>
            </div>
        )
    }
}
function mapStateToProps(state){
	return {
		allAlbums: state.allAlbums
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            loadAlbums: Actions.loadAlbums
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(AlbumsList)));