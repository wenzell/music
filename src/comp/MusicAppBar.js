import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../actions/index.js';


const styles = (theme) =>{
    return({
        root: {
            flexGrow: 1,
        },
        flex: {
            flex: 1,
            fontWeight:'300',
            '@media screen and (max-width:768px)':{
    
            }
        },
        appBar:{
            backgroundColor:'#fff',
            '&:after':{
                "bottom": "-5px",
                "boxShadow": "inset 0px 4px 8px -3px rgba(17, 17, 17, .06)",
                "content": "\"\"",
                "height": "5px",
                "left": "0px",
                "opacity": "1",
                "pointerEvents": "none",
                "position": "absolute",
                "right": "0px",
                "width": "100%",
                "zIndex": "2050"
            },
            '@media screen and (max-width:768px)':{
                position: 'absolute'
            }
        },
        toolbar :{
            paddingLeft:'16px',
            paddingRight:'16px',
            minHeight:'64px'
        },
        iconlink:{
            textDecoration: 'none',
            color:'primary',
            display:'flex',
            flexFlow:'row',
            alignItems:'center',
            '&>img':{
                width:'40px',
                height:'40px',
                marginRight:'5px'
            }
        },
        formcontrol:{
            width:'100%'
        },
        searchbar:{
            backgroundColor:'rgba(0,0,0,0.035)',
            padding:'5px',
            paddingLeft: '10px',
            borderRadius:'5px',
            marginLeft:'auto',
            right:0,
            color:theme.palette.primary.main,
            '&:hover':{
                backgroundColor:'rgba(0,0,0,0.010)',
            }
        },
        input:{
            width:'100px',
            transition:'0.5s ease-in-out',
            right:0,
            '&:focus':{
                width:'300px',
            },
            '@media screen and (max-width:768px)' :{
                width:'25px',
                '&:focus':{
                    width:'200px'
                }
            },
        }
    }
    )
    
};


class MusicAppBar extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            searchActive: false,
            searchQuery: ""
        }
        this.handleSearchFocus = this.handleSearchFocus.bind(this);
    }


    handleSearchFocus = (f) =>{
        this.setState({
            searchActive: f
        })
    }

    handleSearch = (query) =>{
        if(this.props.history.location.pathname!== "/search"){
            this.props.history.push('/search')
        }
            this.props.search(query)


    }

    render(){
        var classes = this.props.classes
        return(
            <div className={classes.root}>
                <AppBar color="default" elevation={0} className={classes.appBar} style={{minHeight:"64px",zIndex:1300}}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton onClick={()=>{this.props.handleDrawer()}} color="primary" aria-label="Menu" style={{marginRight:'5px'}}>
                            <MenuIcon />
                        </IconButton>
                        <Link to="/" className={classes.iconlink} style={{ }}><img alt="INCMusic" src="https://d607d9adb89cabd66253-1422a07285cc35aa1c8781082aaa4295.ssl.cf1.rackcdn.com/icons/icon-incmusic-app.svg" /><Typography classes={{root:classes.flex}} color="primary" variant="title">incmusic</Typography></Link>
                        <FormControl classes={{root:classes.formcontrol}}>
                            <Input onChange={o =>{this.handleSearch(o.target.value)}} onBlur={()=>{this.handleSearchFocus(false)}} onFocus={()=>{this.handleSearchFocus(true)}} disableUnderline id="search-bar" classes={{root:classes.searchbar, input:classes.input}} startAdornment={
                                <InputAdornment position="start">
                                    <SearchIcon/>
                                </InputAdornment>
                            } />
                        </FormControl>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

function mapStateToProps(state){
	return {
		allSongs: state.allSongs
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            search: Actions.search
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(MusicAppBar)));