import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import MusicVideoIcon from '@material-ui/icons/MusicVideo';

import { withTheme, withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import classnames from 'classnames';


const styles= {
    drawer: {
        width:'250px',
        paddingTop:'64px',
        backgroundColor:"#424242",
        transition: '0.3s ease-in-out !important',
        overflow:'hidden',
        '@media screen and (max-width:768px)': {
            width:'100vw',
            height:'100vh',
        },
    },
    open:{
        transform:"translateX(-100vw) !important"
    },
    listText:{
        color:"#a1a1a1",
        textDecoration:'none',
        '&:hover':{
            color:'#a1a1a1'
        }
    }
};

class EventsDrawer extends React.Component{
    constructor(props){
        super(props);

        this.state= {
        }

    }
    render(){
        var classes = this.props.classes;
        return(
            <Drawer classes={{paper:classnames(classes.drawer, !this.props.drawerOpen ? classes.open : '')}}  variant="persistent" open={this.props.drawerOpen}>
                <List>
                    <Link to="/albums" onClick={this.props.handleDrawer} style={{textDecoration:'none',color:'inherit'}}><ListItem button>
                        <ListItemIcon>
                            <LibraryMusicIcon className={classes.listText} />
                        </ListItemIcon>
                        <ListItemText classes={{primary:classes.listText}} primary="Albums" />
                    </ListItem></Link>
                    <Link to="/songs" onClick={this.props.handleDrawer} style={{textDecoration:'none',color:'inherit'}}><ListItem button>
                        <ListItemIcon>
                            <MusicNoteIcon className={classes.listText} />
                        </ListItemIcon>
                        <ListItemText classes={{primary:classes.listText}} primary="Songs" />
                    </ListItem></Link>
                    <a href="https://incmedia.org/category/cmv/"  style={{textDecoration:'none'}} target="_blank" rel="noopener noreferrer">
                    <ListItem button>
                        <ListItemIcon>
                            <MusicVideoIcon className={classes.listText} />
                        </ListItemIcon>
                        <ListItemText classes={{primary:classes.listText}} primary="CMVs" />
                    </ListItem>
                    </a>
                </List>
            </Drawer>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsDrawer));