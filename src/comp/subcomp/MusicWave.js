import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import WaveSurfer from 'wavesurfer.js';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';



const styles= {
    howler:{
        width:'100%',
        height:'100%',
        padding:'15px 30px',
        paddingTop:'0',
        display:'flex',
        flexFlow:'column'
    },
    waveform:{
        '@media screen and (max-width: 768px)':{
            display:'none',
        }
    },
    hr:{
        margin:0,
        marginTop:'-10px',
        opacity:0.3,
        '@media screen and (max-width: 768px)':{
            display:'none',
        }
    },
    len:{
        position:'absolute',
        transform: 'translate(0,-10px)',
        left:'20px',
        fontSize:'0.45em'
    }
};


/*
Song Objects consist of 



idSongs: unique song ID
idGenre: song Genre (pop, rock, rap, etc.) [Not yet used in production]
idCategory: song Category [Not yet used in production]
idLyrics: unique song lyrics id for api/lyrics/:id
idAlbum: corresponding album ID for /api/songs?albumId=:id (songs in album) or /api/albums/:id (album information)
idAlbumContent: unique place in album for ordering
songAuthor: song author [Not yet used in production]
songTitle: Title of song (sometimes songs have same titles)
albumTitle: corresponding album title
artUrl: if album/song has album cover. blank if none
songUrl: url of song file (mp3/wav)
videoUrl: url of video [Not yet used in production]
bpm: tempo [Not yet used in production]
dateRelease: release date
len: length of song [Not yet used in production]

*/
var formatTime = function (time) {return [ Math.floor((time % 3600) / 60),('00' + Math.floor(time % 60)).slice(-2) ].join(':');};

class MusicWave extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            currentTime: '0:00',
            songLength: '0:00',
            currentIndex: 0,
        }
        this.setTime = this.setTime.bind(this);
    }

    componentDidMount = () =>{
        window.wavesurfer = WaveSurfer.create({
            container: '#waveform',
            waveColor: 'grey',
            progressColor: '#c62828',
            backend: 'MediaElement',
            height:'20',
            autoplay:true,
            pixelRatio: '1.5',
            normalize: 'true',
            minPxPerSec: '250',
            barWidth:'1',
        });


        //Player events
        window.wavesurfer.on('audioprocess', ()=>{
            this.setTime()
        })
        window.wavesurfer.on('seek', ()=>{
            this.setTime()
        })
        window.wavesurfer.on('ready', ()=>{
            var currentIndex = this.props.playlist.findIndex(o =>{ return o.idSongs === this.props.currentSong.idSongs})
            this.props.changeIndex(currentIndex)
            this.setState({ currentIndex })
        })
        window.wavesurfer.on('finish', ()=>{
            if(this.props.repeat){
                window.wavesurfer.play()
            }
            else{
                this.props.playSong(this.props.playlist[ this.state.currentIndex === this.props.playlist.length-1 ? 0 : this.state.currentIndex + 1  ])
            }
        })
        
    }



    // Update song time
    setTime = () =>{
        this.setState({
            currentTime: formatTime(window.wavesurfer.getCurrentTime()), 
            songLength: formatTime(window.wavesurfer.getDuration())
        })
    }




    render(){
        var classes = this.props.classes;
        return(
            <div  className={classes.howler}>
                <div className={classes.len}>{this.state.currentTime} | {this.state.songLength === "NaN:aN" ? "0:00" : this.state.songLength}</div>
                <div style={{display:( (window.innerWidth <768) && this.props.drawerUp ? 'block' : 'block')}}  className={classes.waveform} id="waveform"></div>
                <hr style={{display:( (window.innerWidth <768) && this.props.drawerUp ? 'block' : 'block')}} className={classes.hr} />
            </div>
        )
    }
}

function mapStateToProps(state){
	return {
        isPlaying: state.isPlaying,
        currentSong:state.currentSong,
        repeat: state.repeat,
        playlist: state.playlist
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            playSong: Actions.playSong,
            togglePlay: Actions.togglePlay,
            togglePause: Actions.togglePause,
            changeIndex: Actions.changeIndex
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(MusicWave)));