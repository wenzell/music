import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Typography from '@material-ui/core/Typography';

import DragHandleIcon from '@material-ui/icons/DragHandle';

import MusicMenu from './MusicMenu';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';



const styles= {
    drag:{
        fontSize:'1.2em',
        marginRight:'10px',
        opacity:'0',
        transition:'opacity 0.3s',
        '@media screen and (max-width:768px)':{
            opacity: '0.75'
        }
    },
    vert:{
        opacity:'0',
        transition:'opacity 0.3s',
        '@media screen and (max-width:768px)':{
            opacity: '0.75'
        }
    },
    albumimage:{
        width:'40px',
        height:'40px',
        marginRight:'10px'
    },
    song:{
        zIndex:9999,
        '&:hover':{
            '&>$drag, &>$vert':{
                opacity:'0.75'
            }
        }
    },
    songinfo:{
        display:'flex',
        flexFlow:'column',
        justifyContent:'center',
        flex: 1,
        cursor:'pointer'
    },
    songlength:{
        marginLeft:'auto',
        marginRight:'5px'
    },
    songtitle:{
        '& .small':{
            fontSize: '0.85em'
        },
        '@media screen and (max-width:768px)':{
            fontSize: '0.85em'
        }
    },
    albumtitle:{
        '& .small':{
            fontSize: '0.65em'
        },
        '@media screen and (max-width:768px)':{
            fontSize: '0.65em'
        }
    },
    dragcontext:{
        zIndex:9999
    }
};

// fake data generator

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const getItemStyle = (isDragging, draggableStyle, isPlaying) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  margin: `0px -10px`,
  padding:'10px',
  display:'flex',
  alignItems:'center',
  transition:'0.1s',
  // change background colour if dragging
  background:isPlaying ? '#eee' : '#fff',
  boxShadow: isDragging ?'0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)':'',
  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
  background: '#fff',
  width: '100%',
});

class MusicSongGroup extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            items: this.props.album,
            song:{},
        };
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    
    componentDidMount = () =>{
    }

    updateGroup = () =>{
        this.setState(this.state)
    }

    onDragEnd(result) {
        // dropped outside the list
        const { source, destination } = result;
        console.log(source)
        console.log(destination)

        if (!destination) {
            return;
        }


        if(source.droppableId === destination.droppableId){
            if(destination.droppableId === "droppable"){
                return;
            }
            const items = reorder(
                this.props.album,
                result.source.index,
                result.destination.index
            );

            this.props.orderPlaylist(items)
        }
        else{
            const result = move(
                this.props.album,
                this.props.playlist,
                source,
                destination
            )

            this.props.orderPlaylist(result)
        }

    }

    loadSong = (song) =>{
        this.props.playSong(song)
    }

    render(){
        var classes = this.props.classes;
        if(!this.props.album) return null;
        return(
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable isDropDisabled={!this.props.isPlaylist} droppableId={this.props.isPlaylist ? "droppablePlaylist" : "droppable"}>
                {(provided, snapshot) => (
                    <div ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)}>
                        {this.props.album.map((item, index) => (
                            <Draggable key={item.idSongs}  draggableId={item.idSongs} index={index}>
                                {(provided, snapshot) => (
                                    <div ref={provided.innerRef} {...provided.draggableProps} className={classes.song}  style={getItemStyle(snapshot.isDragging,provided.draggableProps.style,(this.props.currentSong.idSongs === item.idSongs && this.props.isPlaylist))}>
                                        <DragHandleIcon className={classes.drag} {...provided.dragHandleProps} />
                                        <img alt={item.albumTitle} src={item.artUrl !== "" ? item.artUrl : window.noArtUrl } onError={(e)=>{e.target.src=window.noArtUrl}}  className={classes.albumimage} style={this.props.small? {width:'30px',height:'30px'}: {}}/>
                                        <div onClick={()=>{this.loadSong(item)}} className={classes.songinfo}>
                                            <Typography className={classes.songtitle} style={this.props.small? {fontSize:'0.85em'}: {}} variant="subheading">{item.songTitle}</Typography>
                                            <Typography className={classes.albumtitle} style={this.props.small? {fontSize:'0.65em'}: {}} variant="caption">{item.albumTitle}</Typography>
                                        </div>
                                        <Typography variant="caption" className={classes.songlength}></Typography>
                                        <MusicMenu updateGroup={this.updateGroup} isPlaylist={this.props.isPlaylist}  className={classes.vert} song={item} playlistIndex={index} />
                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
                </Droppable>
            </DragDropContext>
        )
    }
}

MusicSongGroup.defaultProps={
    small: false,
}

function mapStateToProps(state){
	return {
        isPlaying: state.isPlaying,
        playlist: state.playlist,
        currentSong: state.currentSong
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            playSong: Actions.playSong,
            togglePlay: Actions.togglePlay,
            togglePause: Actions.togglePause,
            orderPlaylist: Actions.orderPlaylist
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(MusicSongGroup)));