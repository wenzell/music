import React from 'react';
import classnames from 'classnames';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const styles= {
    root:{
        width:'180px',
        '@media screen and (max-width:768px)':{
            width: '100%'
        },
        '@media screen and (min-width:1454px)':{
            width:'250px'
        }
    },
    paper:{
        width:'100%',
    },
    image:{
        width:'100%',
        height:'180px',
        backgroundSize: 'cover',
        marginBottom:'10px',
        transition:'300ms',
        cursor:'pointer',
        '&:hover':{
            filter:'brightness(0.85)'
        },
        '@media screen and (max-width:768px)':{
            height:'41.25vw'
        },
        '@media screen and (min-width:1454px)':{
            height:'250px'
        }
    },
    albumTitle:{
        display:'flex',
        flexFlow:'column',
        padding:'10px 10px 10px 0px',
        paddingTop:'0'
    },
    ttitle:{
        marginLeft:'0px',
        lineHeight:'1.2em',
    },
    icon:{
        fontSize:'0.8em'
    }
};

class MusicAlbumCard extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <Link to={'/albums/'+this.props.album.id} style={{textDecoration : 'none'}}>
            <div className={classnames(classes.root,this.props.classz)}>
                <Paper  classes={{root:classes.paper}} className="paper" elevation={1}>
                    <div   style={{backgroundImage:'url('+ this.props.album.artUrl + '),url('+window.noArtUrl+')'}} onError={ o =>{ console.log(this);this.style= {backgroundImage: "url(https://d607d9adb89cabd66253-1422a07285cc35aa1c8781082aaa4295.ssl.cf1.rackcdn.com/music/incmusic-default-album250px.png)" } }} className={classes.image} >
                    </div>
                    
                </Paper>
                <div className={classes.albumTitle}>
                    <Typography noWrap className={classes.ttitle} variant="body2">{this.props.album.title}</Typography>
                    <Typography className={classes.tyear} variant="caption">2014 - 2018</Typography>
                </div>
            </div>
            </Link>
        )
    }
}

export default withTheme()(withStyles(styles)(MusicAlbumCard));