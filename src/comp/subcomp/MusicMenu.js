import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';

import MoreVertIcon from '@material-ui/icons/MoreVert';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/index.js';

function move(arr, old_index, new_index) {
    while (old_index < 0) {
        old_index += arr.length;
    }
    while (new_index < 0) {
        new_index += arr.length;
    }
    if (new_index >= arr.length) {
        let k = new_index - arr.length;
        while ((k--) + 1) {
            arr.push(undefined);
        }
    }
     arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
   return arr;
}

const styles= {
    
};

class MusicMenu extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            songInfoMenu:null,
        }
    }

    

     //Menu Open/Close functions
    handleSongMenu = (event) =>{
        this.setState({ songInfoMenu: event.currentTarget });
    }

    handleAddToPlaylist = () =>{
        this.props.addPlaylist([this.props.song])
        this.handleSongMenuClose()
    }

    handlePlaylistOrder = (order) =>{
        if(order === 1){
            if(this.props.playlistIndex === this.props.playlist.length-1){
                this.handleSongMenuClose()
                return null;
            }
            else{
                this.props.orderPlaylist(move(this.props.playlist, this.props.playlistIndex, this.props.playlistIndex + 1))
            }
        }else if(order === -1){
            if(this.props.playlistIndex === 0){
                this.handleSongMenuClose()
                return null;
            }
            else{
                this.props.orderPlaylist(move(this.props.playlist, this.props.playlistIndex, this.props.playlistIndex - 1))
            }
        }
        else{
            if(this.props.playlist.length === 1){
                this.props.clearPlaylist();
                this.props.updateGroup()
                this.handleSongMenuClose()
                return null;
            }
            const newArray = this.props.playlist;
            newArray.splice(this.props.playlistIndex, 1);
            if(this.props.currentSong.idSongs === this.props.song.idSongs){
                this.props.playSong(this.props.playlistIndex === newArray.length ? newArray[this.props.playlistIndex] : newArray[0])

            }
            this.props.orderPlaylist(newArray);
        }
        this.props.updateGroup()
        this.handleSongMenuClose()

    }

    handleSongMenuClose= () =>{
        this.setState({ songInfoMenu: null });
    }

    handleLyrics = () =>{
        this.props.showLyrics(this.props.song.idLyrics)
        this.handleSongMenuClose()
    }

    handleClearPlaylist = () =>{
        this.props.clearPlaylist()
        this.handleSongMenuClose()
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.menu + ' ' + this.props.className}>
                <IconButton color={this.props.color} onClick={this.handleSongMenu}>
                    <MoreVertIcon />
                </IconButton>
                {this.props.playlistControl ? 
                    <Menu onClose={this.handleSongMenuClose} anchorEl={this.state.songInfoMenu} open={Boolean(this.state.songInfoMenu)}>
                        <MenuItem disabled={this.props.playlist.length === 0} onClick={this.handleClearPlaylist}>Clear playlist</MenuItem>
                    </Menu>
                    :
                    <Menu onClose={this.handleSongMenuClose} anchorEl={this.state.songInfoMenu} open={Boolean(this.state.songInfoMenu)}>
                        {this.props.song.idLyrics !== null ? <MenuItem onClick={this.handleLyrics}>Lyrics</MenuItem> : null}
                        <a style={{textDecoration:'none'}} href={this.props.song.songUrl} target="_blank" download onClick={this.handleSongMenuClose}><MenuItem >Download this song</MenuItem></a>
                        {(!this.props.isPlaylist && !this.props.playerSong? (<MenuItem disabled={this.props.playlist.some(o=>{return o.idSongs === this.props.song.idSongs})} onClick={this.handleAddToPlaylist}>Add to playlist</MenuItem>) : null)}
                        {(this.props.isPlaylist ? (<MenuItem disabled={this.props.playlistIndex === 0} onClick={o=>{this.handlePlaylistOrder(-1)}}>Move up playlist</MenuItem>) : null)}
                        {(this.props.isPlaylist ? (<MenuItem disabled={this.props.playlistIndex === (this.props.playlist.length-1)} onClick={o=>{this.handlePlaylistOrder(1)}}>Move down playlist</MenuItem>) : null)}
                        {(this.props.isPlaylist ? (<MenuItem onClick={o=>{this.handlePlaylistOrder(0)}}>Remove from playlist</MenuItem>) : null)}         
                    </Menu>
                }
            </div>
            
        )
    }
}

MusicMenu.defaultProps = {
    color : 'default'
}


function mapStateToProps(state){
	return {
        isPlaying: state.isPlaying,
        currentSong: state.currentSong,
        playlist: state.playlist,
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            showLyrics: Actions.showLyrics,
            addPlaylist: Actions.addPlaylist,
            playSong: Actions.playSong,
            clearPlaylist: Actions.clearPlaylist,
            orderPlaylist: Actions.orderPlaylist,
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(MusicMenu)));