import React from 'react';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Modal from '@material-ui/core/Modal';
import * as Actions from '../../actions/index.js';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';


const styles= {
    paper:{
        top:'2.5%',
        bottom:'2.5%',
        left:'20%',
        right:'20%',
        position:'absolute',
        background:'#fff',
        padding:'20px',
        maxHeight:'95vh',
        overflow:'auto',
        '@media screen and (max-width:768px)':{
            left:'10%',
            right:'10%',
            transform:'initial',
            maxHeight:'initial'
        }
    },
    lyrics:{
        textAlign:'center',
        fontWeight:300,
        fontSize:'0.95em',
        fontFamily:'Roboto'
    },
    close:{
        width:'100%',
        textAlign:'right'
    }
};

class LyricsModal extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            idLyrics: this.props.idLyrics,
            hhtml : ""
        }
    }

    handleClose = () =>{
        this.setState({hhtml: ""})
        this.props.hideLyrics()
    }

    handleOpen = () =>{
        axios.get(window.rootUrl + 'lyrics/' + this.props.idLyrics).then(res=>{
            this.setState({hhtml: res.data[0].contents})
        })
    }


    render(){
        var classes = this.props.classes;
        return(
        <Modal BackdropProps={{style:{willChange:"initial"}}} aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={this.props.lyricsOpen} onRendered={this.handleOpen} onClose={this.handleClose}>
            <div className={classes.paper}>
                <div className={classes.close} onClick={this.handleClose}>
                    <IconButton>
                        <CloseIcon />
                    </IconButton>
                </div>
                <div className={classes.lyrics} id="simple-modal-description" dangerouslySetInnerHTML={{__html:this.state.hhtml}}>
                </div>
            </div>
          </Modal>
        )
    }
}

function mapStateToProps(state){
	return {
        lyricsOpen: state.lyricsOpen,
        idLyrics: state.idLyrics,
        lyricsHTML: state.lyricsHTML
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators(
		{
            hideLyrics: Actions.hideLyrics
		}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(withTheme()(withStyles(styles)(LyricsModal)));