import React from 'react';
import classnames from 'classnames';
import { withTheme, withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
import Helmet from 'react-helmet';
import Home from './routes/Home';
import SearchPage from './routes/SearchPage';
import AlbumPage from './routes/AlbumPage';
import AlbumsList from './routes/AlbumsList';
import SongsList from './routes/SongsList';

const styles= {
    eventsbrowser: {
        width:'100%',
        marginTop: '64px',
        display:'flex'
    },
    aside:{
        width:'320px',
        transition:'0.4s ease-in-out',
        '&.active':{
            width:'0',
            transition:'0.3s ease-in-out'
        }
    },
    eventcontainer : {
        flex:1,
        width:'100%',
        right:0,
        transition:'0.4s ease-in-out',
    },
    content:{
        flexGrow: 1,
        display:'flex',
        flexFlow: 'column',
        padding:'30px ',
        width:'100%',
        paddingRight:'320px',
        '@media screen and (max-width:768px)':{
            padding:'20px',
            paddingBottom:'250px'
        }
    }
    
};

class EventsBrowser extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            
        }
    }

    render(){
        var classes = this.props.classes;
        return(
            <div className={classes.eventsbrowser }  >
                <Helmet>
                    <title>INCMusic</title>
                </Helmet>
                <div className={classnames(classes.eventcontainer,(this.props.drawerOpen ?'active':''))}>
                <div className={classes.content}>
                    <Switch >
                        <Route path="/search" component={SearchPage} />
                        <Route exact path="/" component={Home} />
                        <Route path="/albums/:albumid" component={AlbumPage} />
                        <Route exact path="/albums" component={AlbumsList}/>
                        <Route exact path="/songs" component={SongsList}/>
                    </Switch>
                </div>
                
                </div>
            </div>
        )
    }
}

export default withTheme()(withStyles(styles)(EventsBrowser));